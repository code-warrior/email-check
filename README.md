# Email Check Using Ajax, PHP, and MySQL

v1.0.0

This web app takes an email from a user twice inside an HTML form, compares the first entry to the second, then checks whether it already exists in a database. If it does, the user is notified inside the form’s input field. Otherwise, the app continues without incident.

This web app is…

- CSS3 compliant
- HTML5 compliant
- JSLint compliant
- ESLint compliant
- PHP Code Sniffer compliant

## Video Demo

You can [see a 1-minute video](https://www.youtube.com/watch?v=45GThJaRpek) of how this project works on [YouTube](https://www.youtube.com/watch?v=45GThJaRpek).

## Installation Instructions

### Compiling The CSS from Sass

Although you needn’t any CSS to work with this project, you may want to compile the CSS from the Sass for a heightened visual experience. Run the following from the root folder of this project:

        sass --unix-newlines --sourcemap=none --style compressed --watch sass/style.scss:css/style.css

### Configuring The Database for All Operating Systems

1. Log in to MySQL as root

        $ mysql -u root -p

2. Create a database called `testemail`

        mysql> CREATE DATABASE testemail DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

3. Use the new database

        mysql> USE testemail

4. Create a user called `e1nojuhan1`, give her a password `Rau4avaar@`, then grant her access to all the tables in the new database

        mysql> GRANT ALL PRIVILEGES ON testemail.* TO 'e1nojuhan1'@'localhost' IDENTIFIED BY 'Rau4avaar@';

5. Create a basic table called `users` with an field called `email`

        mysql> CREATE TABLE users (email VARCHAR(45) PRIMARY KEY ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

6. Add four entries into the new table

        mysql> INSERT INTO users (email) VALUES ("a@a.com"),("b@b.com"),("c@c.com"),("d@d.com");

### Mac Installation

1. Place the `email-check` folder inside your `Sites` folder.

2. Point a browser at the `email_check` folder. In my case, for example, I point Firefox at `localhost/~royvanegas/email-check`

3. Open The Terminal, navigate to the `Sites` folder, and give `email_check` `755` access

        chmod 755 email_check

4. Change directory into the `email_check` folder

        cd email_check

5. Give all the enclosed files `755` access

        chmod 755 *

### Windows Installation (WAMPServer)

1. Place the `email-check` folder inside your `www` folder, which is inside your `wamp` folder.

2. Point a browser at the `email_check` folder: `localhost/email-check`

3. Open Cygwin, navigate to the `wamp` folder, and give `email_check` `755` access

        chmod 755 email_check

4. Change directory into the `email_check` folder

        cd email_check

5. Give all the enclosed files `755` access

        chmod 755 *
