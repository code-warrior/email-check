<?php
/**
 * Contains a public function that prints one of three HTML containers that places
 * the email check app in one of two states: valid or invalid. See the function
 * comment below for more.
 *
 * PHP version 5.3.28
 *
 * @category Default
 * @package  Default
 * @author   Roy Vanegas <roy@thecodeeducators.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://roy.vanegas.org Roy Vanegas
 */

/**
 * POPULATE BODY WITH
 *
 * Prints a container with the email form if a successful connection to the
 * database is made. Otherwise, prints one of two containers indicating the
 * nature of the MySQL-based error.
 *
 * @param Integer $option  defines which container is printed by the switch
 *                         statement.
 * @param String  $message is only used by the DATABASE_CONNECTION_ERROR option.
 *
 * @return void
 */
function populateBodyWith($option, $message = null)
{
    include_once "defines.php";

    switch($option) {
    case EMAIL_FORM:
        echo '<main>
        <h1>Check Email Availability</h1>
        <form>
           <p>Email <input type="email" autofocus
              placeholder="eg, rms@website.com" id="email1"></p>
           <p>Email (again) <input type="email"
              placeholder="eg, rms@website.com" id="email2"></p>
        </form>
        <p>The contents in the second email field are verified as a match
           against the contents in the first email field. Tab-ing away from the
           second field generates an Ajax call that checks the database for
           your email. If found, you will be notified. Otherwise, your email
           will remain inside the text fields, ready for further processing.</p>
        <script src="js/email_check.js"></script>
     </main>';

        break;

    case DATABASE_QUERY_ERROR:
        echo '<main id="error">
        <h1>An Error Has Occurred With The Database!</h1>
        <p>There are many reasons why this error was generated. Inspect the
           following possibilities:</p>
        <ul>
           <li>Is the variable in your <code><abbr
               title="PHP Data Objects">PDO</abbr></code> object named
               <code>dbname</code>?</li>
           <li>Does the prepared statement containing the MySQL <code>SELECT</code>
               query contain any typos?</li>
           <li>Was the <code>testemail</code> database created?</li>
           <li>Was the <code>users</code> table inside the database
                created?</li>
           <li>Is the user accessing the database named
               <code>e1nojuhan1</code>?</li>
           <li>Is the password for the user <code>Rau4avaar@</code>?</li>
           <li>Are you connecting to the database on
               <code>localhost</code>?</li>
        </ul>
     </main>';

        break;

    case DATABASE_CONNECTION_ERROR:
        echo '<main id="error">
        <h1>Could Not Connect to The Database!</h1>
        <p>The following error was generated when trying to connect to the
           database:</p>
        <pre>' . $message . '</pre>
        <p>Here are a few questions to ask in diagnosing this problem.</p>
        <ul>
           <li>Is the MySQL server turned on?</li>
           <li>Has the MySQL database been created?</li>
           <li>Are you connecting on the correct socket?</li>
           <li>Are there any typos in any of the <code>new PDO</code>
               statements?</li>
        </ul>
     </main>';

        break;

    default:
        return;
    }
}
