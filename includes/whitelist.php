<?php
/**
 * Contains a white list function that ensures a $_GET-based form’s variables are
 * handled securely.
 *
 * PHP version 5.3.28
 *
 * @category Default
 * @package  Default
 * @author   Roy Vanegas <roy@thecodeeducators.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://roy.vanegas.org Roy Vanegas
 */

/**
 * WHITE LIST
 *
 * Returns true if this white list verifies its predefined variables against a
 * $_POST variables’ form values, or, false otherwise.
 *
 * @param Array $form_input_names is an array that represents the elements in a
 *              form that must be verified.
 *
 * @return Boolean $state represents whether this function was successful in
 *                 matching the values in the $form_input_names array against the
 *                 $key values submitted in the $_GET array.
 */

function whiteList( $form_input_names )
{
    $state = false;

    $amount_of_form_input_names_found_in_whitelist = 0;

    if (isset($_GET)) {
        foreach ($_GET as $key => $value) {
            if (in_array($key, $form_input_names)) {
                $amount_of_form_input_names_found_in_whitelist++;
            }
        }

        if (count($_GET) == $amount_of_form_input_names_found_in_whitelist) {
            $state = true;
        }
    }

    return $state;
}
