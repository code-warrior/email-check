/*jslint browser: true, devel: true, maxlen: 85 */
/*global window */

window.onload = function () {
    'use strict';

    var xmlhttp,
        email1 = document.getElementById('email1'),
        email2 = document.getElementById('email2');

    /**
     * EMAILS MATCH
     */
    function emails_match() {

        var match = true;

        if (email1.value === email2.value) {
            email1.setAttribute('style', 'background-color: #fff; color: #555');
            email2.setAttribute('style', 'background-color: #fff; color: #555');
        } else {
            email1.value = 'Emails don’t match!';
            email2.value = 'Emails don’t match!';

            email1.setAttribute('style', 'background-color: #900; color: #fff');
            email2.setAttribute('style', 'background-color: #900; color: #fff');

            match = false;
        }

        return match;
    }

    /**
     * INITIATE XML RESPONSE
     *
     * This function doesn’t return anything. It’s triggered by AJAX’s
     * onreadystatechange property.
     */
    function initiate_xml_response() {

        /*
         If all the data has been received and is complete (4) and status is ok
         (200), retrieve the AJAX call’s response.
         */
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            var xml_response = xmlhttp.responseText;

            if (xml_response === 'Already exists') {
                email1.setAttribute('style', 'background-color: #900; color: #fff');
                email2.setAttribute('style', 'background-color: #900; color: #fff');
            }

            // Write the response to both email fields.
            email1.value = xml_response;
            email2.value = xml_response;
        }
    }

    /**
     * START AJAX COMMUNICATION
     */
    function start_ajax_communication() {

        // If emails do not match, return before trying anything else.
        if (!emails_match()) {
            return;
        }

        if (!xmlhttp) {
            xmlhttp = new XMLHttpRequest();
        }

        var url = 'email_check.php?email=' + email2.value;

        xmlhttp.open('GET', url, true);
        xmlhttp.onreadystatechange = initiate_xml_response;
        xmlhttp.send(null);
    }

    /**
     * MANAGE EVENT
     */
    function manage_event(eventObj, event, eventHandler) {

        // Attach the listeners to the email field — on standards browsers.
        if (eventObj.addEventListener) {
            eventObj.addEventListener(event, eventHandler, false);
        } else {
            // But this is how it’s done for that damned IE browser.
            if (eventObj.attachEvent) {
                event = 'on' + event;
                eventObj.attachEvent(event, eventHandler);
            }
        }
    }

    manage_event(email2, 'blur', start_ajax_communication);
};
