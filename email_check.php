<?php
/**
 * Checks the database for the existence of an email that is submitted by the
 * user, echoing either “Already Exists!” (the fail condition) or the originally-
 * entered email (the success condition) back to the submitted form’s text fields.
 *
 * PHP version 5.3.28
 *
 * @category Default
 * @package  Default
 * @author   Roy Vanegas <roy@thecodeeducators.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://roy.vanegas.org Roy Vanegas
 */

define('ABSPATH', dirname(__FILE__) . '/');

require_once ABSPATH . "includes/whitelist.php";

$valid_GET_items = array('email');

if (whiteList($valid_GET_items)) {
    if (isset($_GET['email'])) {
        if (strlen($_GET['email']) > 0) {
            $email = htmlentities(
                trim($_GET['email']),
                ENT_QUOTES | 'ENT_HTML5', "UTF-8"
            );

            // No comments in emails. Evil.
            if (strpos($email, "--") !== false) {
                return;
            }

            // Perhaps someone is trying to submit multiple queries. Evil.
            if (strpos($email, ";") !== false) {
                return;
            }

            try {
                include_once ABSPATH . "includes/config.php";

                $the_db = new PDO(
                    "mysql:host=" . DATABASE_HOSTNAME . ";dbname=" . DATABASE_NAME,
                    DATABASE_USERNAME,
                    DATABASE_PASSWORD
                );

                $statement = $the_db->prepare(
                    "SELECT email FROM users WHERE email=:email"
                );
                $statement->execute(array(':email' => $email));

                while ($row = $statement->fetch()) {
                    $result = $row["email"];
                }

                $statement = null;
            }
            catch(PDOException $error) {
                die();
            }

            if (isset($result)) {
                if ($result == $email) {
                    echo "Already exists";
                }
            } else {
                echo $email;
            }
        }
    }
}
