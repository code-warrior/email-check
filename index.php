<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <title>Check Email Availability (v1.0.0)</title>
      <link rel="stylesheet" href="css/style.css">
   </head>
   <body>
<?php
/**
 * The front-facing page that populates the <body> element with either the
 * intended email form or an error message that notifies the user of a problem
 * connecting to the MySQL database.
 *
 * PHP version 5.3.28
 *
 * @category Default
 * @package  Default
 * @author   Roy Vanegas <roy@thecodeeducators.com>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://roy.vanegas.org Roy Vanegas
 */

try {
    include_once "includes/config.php";
    include_once "includes/output.php";
    include_once "includes/defines.php";

    /*
     * TODO: verify that all variables exist before attempting a connection to
     * the database.
     */

    $the_db = new PDO(
        "mysql:host=" . DATABASE_HOSTNAME . ";dbname=" . DATABASE_NAME,
        DATABASE_USERNAME,
        DATABASE_PASSWORD
    );

    $statement = $the_db->prepare("SELECT * FROM users");

    $result = $statement->execute();

    if (true == $result) {
        populateBodyWith(EMAIL_FORM);
    } else {
        populateBodyWith(DATABASE_QUERY_ERROR);
    }

    $statement = null;

} catch(PDOException $error) {
    populateBodyWith(DATABASE_CONNECTION_ERROR, $error);
}
?>

   </body>
</html>
